<a name="unreleased"></a>
## [Unreleased]


<a name="v1.1.0-rc.1"></a>
## [v1.1.0-rc.1] - 2019-04-09
### Feature
- **loading:** add loading bar to landing page


<a name="v1.1.0-rc.0"></a>
## [v1.1.0-rc.0] - 2019-03-24
### Feature
- **query:** add changelog query for front and back
- **volunteer:** improve volunteer design

### Improving application
- **axios:** try to caches response message
- **cache:** applied cache to axios request
- **changelog:** add generate changelog file
- **page:** add changelog for frontend website
- **style:** update content style and p tag


<a name="v1.1.0-beta.5"></a>
## [v1.1.0-beta.5] - 2019-03-24
### Feature
- **release:** full page of project
- **work:** add ui for works and design

### Fixes Bug
- **theme:** fixes theme error on change message

### Improving application
- **color:** change primary color more
- **font:** improve font in project page
- **project:** add duration time for project with end date
- **project:** change badge layout in project page


<a name="v1.1.0-beta.4"></a>
## [v1.1.0-beta.4] - 2019-03-22
### Feature
- **landing:** add floating to top, add result saved all page

### Improving application
- **ux:** make response data and selected available in vuex


<a name="v1.1.0-beta.3"></a>
## [v1.1.0-beta.3] - 2019-03-21
### Fixes Bug
- **image:** change image to serve with https instead

### Improving application
- **link:** project link must open new page
- **project:** add gallery to image and changes banner able
- **project:** add summary and credit of icon
- **project:** add summary to project page
- **project:** add start and end date


<a name="v1.1.0-beta.2"></a>
## [v1.1.0-beta.2] - 2019-03-20
### Feature
- **projects:** implement first version of project page

### Fixes Bug
- **css:** reduce css apply error in scoped css

### Improving application
- **generate:** add works projects volunteers to generate task
- **meta:** add more seo meta data and more
- **project:** add tag and technical badge
- **theme:** add dark mode to notification bar


<a name="v1.1.0-beta.1"></a>
## [v1.1.0-beta.1] - 2019-03-19
### Feature
- **query:** add query all models in apis

### Improving application
- **convert:** support convert education and languages
- **detail:** add detail page and link to landing
- **landing:** add hover only div with link
- **query:** add education as queryable
- **theme:** add dark mode support in card component
- **theme:** add theme button
- **translatable:** add education keyword
- **typo:** helper will show only name of matches str


<a name="v1.1.0-beta.0"></a>
## [v1.1.0-beta.0] - 2019-03-18
### Feature
- **theme:** improve a lot in theme render
- **theme:** integrate theme in to website for a first time

### Fixes Bug
- **color:** background to full in display message page
- **query:** language changes error when setting in dark mode
- **query:** fixes error query in settings configuration
- **translatable:** duplicate title in help page

### Improving application
- **cookie:** save theme and language to cookie
- **theme:** dynamic link color and error theme changes
- **translatable:** change a little page layout
- **ui:** improve link element and title result


<a name="v1.0.0-rc.3"></a>
## [v1.0.0-rc.3] - 2019-03-18
### Fixes Bug
- **notification:** improve noti size in computer and mobile

### Improving application
- **time:** change default time to be UTC format
- **translatable:** improve translatable field in noti


<a name="v1.0.0-rc.2"></a>
## v1.0.0-rc.2 - 2019-03-17
### Feature
- **alot:** translate improvement, display page and dev name
- **css:** add card layout to list of tag and technical
- **grid:** manually implement grid layout base on flex
- **init:** start new project
- **landing:** implement autosuggestion input field
- **many:** Add a lot of new features
- **multi:** animation, i18n and app info
- **noti:** add notification to not found result
- **search:** add support to tag and technical query
- **translate:** improve translatable fields and add social
- **translate:** add locale to searching panel and searchbar
- **translate:** make title to translatable
- **ui:** implement landing page and more
- **ux:** add scrolling to id and error page

### Fixes Bug
- **notification:** change notification size in mobile
- **query:** forget to update query due to code changes
- **ui:** grid ui in index page not full page

### Improving application
- **404:** add 404 page and anything error in page
- **api:** improve limit of list all tag and technical
- **cors:** add access all domain
- **css:** improve some layout on index page
- **downloadable:** add resume pdf link to search query
- **error:** add page to handler external error
- **event:** selected event choose emit only when user selected
- **focus:** make auto focus to input in index page
- **font:** changes font type in some title and content
- **font:** integrate with noto thai font
- **ga:** add google analytic
- **logic:** change a little logic and remove placeholder
- **logo:** get personal logo
- **message:** change help message to vue augo suggestion
- **meta:** config pwa and description string
- **responsive:** make index page support to mobile device
- **responsive:** reduce margin on touch device in help page
- **search:** integrate with models e.g. project, works etc.
- **translate:** add generally translate config
- **translate:** make translate page with input
- **translate:** add translate placeholder
- **translate:** improve help page base on previous languages
- **typo:** add content in help page, English only
- **typo:** add help typo
- **ui:** add code format to classes and content div
- **ui:** add blue hover instead in selection list
- **ux:** support selected query in mobile device


[Unreleased]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-rc.1...HEAD
[v1.1.0-rc.1]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-rc.0...v1.1.0-rc.1
[v1.1.0-rc.0]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.5...v1.1.0-rc.0
[v1.1.0-beta.5]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.4...v1.1.0-beta.5
[v1.1.0-beta.4]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.3...v1.1.0-beta.4
[v1.1.0-beta.3]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.2...v1.1.0-beta.3
[v1.1.0-beta.2]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.1...v1.1.0-beta.2
[v1.1.0-beta.1]: https://gitlab.com/kamontat/frontport/compare/v1.1.0-beta.0...v1.1.0-beta.1
[v1.1.0-beta.0]: https://gitlab.com/kamontat/frontport/compare/v1.0.0-rc.3...v1.1.0-beta.0
[v1.0.0-rc.3]: https://gitlab.com/kamontat/frontport/compare/v1.0.0-rc.2...v1.0.0-rc.3
