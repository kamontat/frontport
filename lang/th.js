import general from './th/general'
import help from './th/help'
import type from './th/type'
import keyword from './th/keyword'
import message from './th/message'
import sentence from './th/sentence'

export default {
  ...general,
  ...keyword,
  ...help,
  ...type,
  ...message,
  ...sentence
}
