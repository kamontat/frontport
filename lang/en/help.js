export default {
  helpMessage: "type '@:help' for more information",
  helpTitle: 'Welcome to help pages!',
  abstract:
    'This website might a little hard to navigate if you open this site in the first time but after you learn in this page, you will change your mind.',
  helpContents: [
    {
      id: 'how-to-get-the-information',
      title: 'How to get the information',
      description: `My website was heavily inspired by the Google search engine concept. Almost everything you will find in the input bar (which I called 'searchbar'). You can do such as get the project that I ever have done before or ever you use for changing languages of website or theme (coming soon). The query language will depend on the language of the site for example if you choose the website to be English, you might need to search something like 'resume' or 'social' but if you choose Thai as the primary language you might need to use 'เรซูเม่' or 'สื่อสังคม' instead. Every query is searched as case insensitive.`
    },
    {
      id: 'query-syntax',
      title: 'Query syntax',
      description: `A searchable text will include 2 searchable keywords. The first keyword is a title (or name) of the element, this represents the information of an individual element which means this must be unique. The second keyword is the tag of the element, which will present the category of each element. All input query must contain at least one or two keywords. The format of output element will be 'title (tag)' and the selection bar will select the only element that matches by 'start with'.`
    },
    {
      id: 'available-keywords',
      title: 'Available keywords',
      description: `I separate tags into 3 categories. The first one is information tags, which contain all about the information of myself the example for this kind of tags is 'Software', 'Website', 'Work' etc. The second type of tag is website settings for example 'language' or 'theme'. The last one is the website information tags. It is different from the first one because this will contains the information that concerned to the website for example 'help', 'application' etc.`
    },
    {
      id: 'faq',
      title: 'FAQ',
      description: ``
    }
  ]
}
