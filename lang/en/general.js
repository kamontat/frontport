export default {
  home: 'Home',
  placeholder: 'What do you want to know ?',
  startdate: 'Start date',
  enddate: 'End date',
  start: 'Start',
  end: 'End'
}
