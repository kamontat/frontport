export default {
  downloadable: 'downloadable',
  translatable: 'translatable',
  technique: 'technique',
  tag: 'tag',
  helper: 'Helper',
  'social-media': 'social media',
  setting: 'Setting',
  model: 'Model'
}
