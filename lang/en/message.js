export default {
  title: {
    important: 'Important message',
    warn: 'Warning message',
    error: 'Error message',
    information: 'Information message'
  },
  message: {
    notfound: '{title} is not found in {suffix}',
    'website-update': 'the website was update from v{old} to v{new}'
  }
}
