import general from './en/general'
import help from './en/help'
import type from './en/type'
import keyword from './en/keyword'
import message from './en/message'
import sentence from './en/sentence'

export default {
  ...general,
  ...keyword,
  ...help,
  ...type,
  ...message,
  ...sentence
}
