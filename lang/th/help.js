export default {
  helpMessage: "พิมพ์ '@:help' เพื่อเปิดหน้าช่วยเหลือ",
  helpTitle: 'สวัสดีสู่หน้าช่วยเหลือ',
  abstract:
    'เว็บไซต์นี้อาจจะเข้าถึงยากในกรณีที่คุณเข้าครั้งแรก แต่หลังจากคุณได้ลองอ่านหน้านี้ คุณจะเปลื่ยนใจ',
  helpContents: [
    {
      id: 'how-to-get-the-information',
      title: 'ทำอย่างไรถึงจะได้ข้อมูลที่ต้องการ',
      description: ''
    },
    {
      id: 'query-syntax',
      title: 'รูปแบบของข้อมูล',
      description: ''
    },
    {
      id: 'available-keywords',
      title: 'คำสำคัญที่มีในระบบ',
      description: ''
    },
    {
      id: 'faq',
      title: 'คำถามที่พบบ่อย',
      description: ``
    }
  ]
}
