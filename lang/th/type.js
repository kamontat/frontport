export default {
  downloadable: 'ดาวน์โหลด',
  translatable: 'แปลภาษา',
  technique: 'เทคนิค',
  tag: 'แท๊ก',
  helper: 'ช่วยเหลือ',
  'social-media': 'สื่อสังคม',
  setting: 'ตั้งค่า',
  model: 'โมเดล'
}
