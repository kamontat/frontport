export default {
  home: 'หน้าแรก',
  placeholder: 'คุณต้องการที่จะรู้อะไร',
  startdate: 'วันที่เริ่ม',
  enddate: 'วันที่เสร็จ',
  start: 'เริ่ม',
  end: 'เสร็จ'
}
