export default {
  title: {
    important: 'ข้อมูลสำคัญ',
    warn: 'แจ้งเตือน',
    error: 'อันตราย',
    information: 'แจ้งข้อมูล'
  },
  message: {
    notfound: 'หา{title}ไม่เจอใน{suffix}',
    'website-update': 'เว็บมีการอัพเดตข้อมูลจากเวอร์ชั่น {old} ไปยัง {new}'
  }
}
