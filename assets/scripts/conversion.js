export const getImage = (key, info) => {
  if (key === 'projects' || key === 'volunteers' || key === 'works')
    return info.banner

  return undefined
}

export const getTitle = (key, info) => {
  if (key === 'works') return info.company
  if (key === 'volunteers') return info.organization
  if (key === 'projects') return info.name
  if (key === 'interests') return info.name
  if (key === 'skills') return `${info.name} (${info.level})`
  if (key === 'educations') return `${info.institution} (${info.type})`
  if (key === 'socials') return info.network
  if (key === 'languages') return info.name

  return info
}

export const getMessage = (key, info) => {
  if (key === 'works') return info.summary
  if (key === 'volunteers') return info.summary
  if (key === 'projects') return info.summary
  if (key === 'skills') return info.description
  if (key === 'educations') return info.area
  if (key === 'languages') return info.proficiency

  return ''
}

export const getKey = (key, info) => {
  return `${key}-${getTitle(key, info)}-${getMessage(key, info)}`
}

export const haveLink = (key, info) => {
  return (
    key === 'works' ||
    key === 'volunteers' ||
    key === 'projects' ||
    // key === 'educations' ||
    key === 'socials'
  )
}

export const getLink = (key, info) => {
  if (key === 'socials') return window.open(info.url, '_blank')

  return `/${key}/${info.id}`
}
