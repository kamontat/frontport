export const listOfSuggestion = async ctx => {
  const locales = ctx.app.i18n.locales
  const locale = ctx.app.i18n.locale

  const $axios = ctx.$axios
  const store = ctx.store

  let tags = []
  let technicals = []
  const socials = store.state.socials[locale]

  const isExist = (value, lang) => {
    return value && value[lang] && value[lang].length && value[lang].length > 0
  }

  if (
    isExist(store.state.tags, locale) &&
    isExist(store.state.technicals, locale)
  ) {
    tags = store.state.tags[locale]
    technicals = store.state.technicals[locale]
  } else {
    const response = await $axios.$get('list/all/', {
      useCache: true,
      params: {
        limit: 50,
        lang: locale
      }
    })

    const list = response.results

    tags = list.tags
    store.commit('setTags', {
      tags,
      lang: locale
    })

    technicals = list.technicals
    store.commit('setTechnicals', {
      technicals,
      lang: locale
    })
  }

  // {
  //   key: '', // key for query action (might duplicate)
  //   type: '' // type of the value (might duplicate)
  //   name: '' // name for showing in website, should translatable (shouldn't duplicate)
  //   raw: ''  // raw data from query (not required)
  // }

  const values = tags.map(v => ({
    key: 'tag',
    type: 'tag',
    name: v
  }))

  values.push(
    ...technicals.map(v => ({
      key: 'technical',
      type: 'technique',
      name: v
    }))
  )

  values.push(
    ...socials.map(s => ({
      key: 'social',
      type: 'social-media',
      name: s.network,
      raw: s
    }))
  )

  values.push(
    {
      key: 'help',
      type: 'helper',
      name: 'help'
    },
    {
      key: 'resume',
      type: 'downloadable',
      name: 'resume'
    },
    {
      key: 'appinfo',
      type: 'setting',
      name: 'website'
    },
    {
      key: 'developer',
      type: 'setting',
      name: 'developer'
    },
    {
      key: 'changelog',
      type: 'changelog',
      name: 'backend'
    },
    {
      key: 'changelog',
      type: 'changelog',
      name: 'frontend'
    },
    {
      key: 'model',
      type: 'model',
      name: 'project'
    },
    {
      key: 'model',
      type: 'model',
      name: 'work'
    },
    {
      key: 'model',
      type: 'model',
      name: 'interest'
    },
    {
      key: 'model',
      type: 'model',
      name: 'skill'
    },
    {
      key: 'model',
      type: 'model',
      name: 'education'
    },
    {
      key: 'model',
      type: 'model',
      name: 'all'
    }
  )

  const nextLanguage = locales.filter(l => locale !== l.code)[0]
  values.push(
    // This will cause bug if more than 2 languages in the website
    {
      key: nextLanguage.code.toLowerCase(),
      type: 'translatable',
      name: nextLanguage.name.toLowerCase()
    },
    {
      key: 'theme',
      type: 'setting',
      name: `${store.state.nextTheme} mode`
    }
  )

  return values
}

export const listOfActions = () => {
  const language = (self, obj) => {
    self.$router.push(self.switchLocalePath(obj.key))

    const nextLanguage = self.$i18n.locales.filter(
      l => self.$i18n.locale !== l.code
    )[0]

    // update the element text
    self.$store.commit('updateSelected', {
      key: nextLanguage.code.toLowerCase(),
      name: nextLanguage.name.toLowerCase()
    })
  }

  const handlerError = (self, response) => {
    if (response.overallTotal === 0) {
      self.$notify({
        group: 'notification',
        title: self.$t('title.important'),
        text: self.$t('message.notfound', {
          title: self.$t('result'),
          suffix: self.$t('backend')
        }),
        type: 'warn'
      })
      return undefined
    } else {
      return response
    }
  }

  const tags = async (self, obj) => {
    const response = await self.$axios.$get('make/summary/', {
      useCache: true,
      params: {
        who: 'net',
        tag: obj.name,
        exclude: 'social,education,language',
        lang: self.$i18n.locale
      }
    })

    return handlerError(self, response)
  }

  const models = async (self, obj) => {
    const params = {
      who: 'net',
      lang: self.$i18n.locale
    }
    if (obj.name !== 'all') params.only = obj.name

    const response = await self.$axios.$get('make/summary/', {
      useCache: true,
      params
    })

    return handlerError(self, response)
  }

  return {
    tag: (self, obj) => tags(self, obj),
    technical: (self, obj) => tags(self, obj),
    model: (self, obj) => models(self, obj),
    help: self => self.$router.push(self.localePath('help', self.$i18n.locale)),
    th: (self, obj) => language(self, obj),
    en: (self, obj) => language(self, obj),
    resume: self =>
      window.open(`${process.env.baseUrl}/download/resume`, '_blank'),
    social: (self, obj) => window.open(obj.raw.url, '_blank'),
    appinfo: self => {
      self.$notify({
        group: 'notification',
        title: self.$t('title.information'),
        text: `${self.$t('web')}: ${
          process.env.pkg.version
        } (${self.$moment
          .utc(process.env.buildDate)
          .fromNow()}), <br/> ${self.$t('apis')}: ${
          self.$store.state.backend.version
        } (${self.$moment.utc(self.$store.state.backend.date).fromNow()})`,
        type: 'info'
      })
    },
    changelog: (self, obj) => {
      if (obj.name === 'frontend')
        window.open('https://kamontat.net/changelog', '_blank')
      else if (obj.name === 'backend')
        window.open('https://apis.kamontat.net/changelog', '_blank')
    },
    developer: self => {
      self.$notify({
        group: 'notification',
        title: self.$t('developer'),
        text: `${self.$t('web')}: ${process.env.pkg.author}, <br/> ${self.$t(
          'apis'
        )}: ${self.$store.state.backend.developer.name}`,
        type: 'info'
      })
    },
    theme: (self, obj) => {
      self.$store.commit('switchTheme')
      const nextTheme = self.$store.state.nextTheme

      self.$store.commit('updateSelected', {
        name: `${nextTheme} mode`
      })

      self.$cookie.set('kct', self.$store.state.theme, {
        maxAge: 60 * 60 * 24 * 7
      }) // update theme in cookie
    }
  }
}
