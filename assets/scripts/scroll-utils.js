export const hashscrolling = async (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  }

  const findElement = (hash, x) => {
    return (
      document.querySelector(hash) ||
      new Promise((resolve, reject) => {
        if (x > 50) {
          return resolve()
        }
        setTimeout(() => {
          resolve(findElement(hash, ++x || 1))
        }, 100)
      })
    )
  }

  const scrollTo = {
    x: 0,
    y: 0
  }

  if (to.hash) {
    const element = await findElement(to.hash)
    if (element) {
      scrollTo.y = element.offsetTop
    }

    window.scrollTo({
      top: scrollTo.y,
      behavior: 'smooth'
    })
  }

  return scrollTo
}
