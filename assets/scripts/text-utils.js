export const capitalLetter = text => {
  return text.substr(0, 1).toUpperCase() + text.substr(1)
}
