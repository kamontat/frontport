#!/usr/bin/env bash
# shellcheck disable=SC1000,SC2068

# generate by create-script-file v4.0.0
# link (https://github.com/Template-generator/create-script-file/tree/v4.0.0)

# set -x #DEBUG - Display commands and their arguments as they are executed.
# set -v #VERBOSE - Display shell input lines as they are read.
# set -n #EVALUATE - Check syntax of the script but don't execute.

VERSION_FLAGS=("$@")

VERSION="$(npm version --no-git-tag-version ${VERSION_FLAGS[@]})"

gitgo changelog --tag "$VERSION"                       # create changelog
git commit -am "chore(release): update to ${VERSION}"  # commit all file
git tag "$VERSION"                                     # create git tag

git push
git push --tags