export const state = () => ({
  themes: ['light', 'dark'],
  theme: 'light',
  nextTheme: 'dark',
  backend: {},
  information: {},
  tags: {},
  technicals: {},
  socials: {},
  response: {}, // apis response message
  selected: undefined, // select object
  value: '' // query value
})

export const mutations = {
  darkTheme(state) {
    state.theme = 'dark'
    state.nextTheme = 'light'
  },
  lightTheme(state) {
    state.theme = 'light'
    state.nextTheme = 'dark'
  },
  switchTheme(state) {
    state.nextTheme = state.theme
    if (state.theme === 'dark') state.theme = 'light'
    else if (state.theme === 'light') state.theme = 'dark'
  },
  setBackendInformation(state, info) {
    state.backend = info
  },
  setInformation(state, { info, lang }) {
    state.information[lang] = info
  },
  setTechnicals(state, { technicals, lang }) {
    state.technicals[lang] = technicals
  },
  setTags(state, { tags, lang }) {
    state.tags[lang] = tags
  },
  setSocials(state, { socials, lang }) {
    state.socials[lang] = socials
  },
  setResponse(state, response) {
    state.response = response
  },
  setSelected(state, selected) {
    state.selected = selected
  },
  updateSelected(state, { key, name, raw }) {
    if (!state.selected) return

    if (key) state.selected.key = key
    if (name) state.selected.name = name
    if (raw) state.selected.raw = raw
  },
  setValue(state, value) {
    state.value = value
  },
  clearResponse(state) {
    state.response = {}
    state.selected = undefined
    state.value = ''
  }
}

export const actions = {
  async nuxtServerInit({ commit }, ctx) {
    const promiseA = ctx.$axios
      .$get('information/net/', {
        useCache: true
      })
      .then(info => {
        commit('setInformation', {
          info: info,
          lang: 'en'
        })
      })

    const promiseB = ctx.$axios
      .$get('information/net/', {
        params: {
          lang: 'th'
        }
      })
      .then(info => {
        commit('setInformation', {
          info: info,
          lang: 'th'
        })
      })

    const promiseC = ctx.$axios
      .$get('socials/', {
        params: {
          who: 'net',
          lang: 'en'
        }
      })
      .then(social => {
        commit('setSocials', {
          socials: social.results,
          lang: 'en'
        })
      })

    const promiseD = ctx.$axios
      .$get('socials/', {
        params: {
          who: 'net',
          lang: 'th'
        }
      })
      .then(social => {
        commit('setSocials', {
          socials: social.results,
          lang: 'th'
        })
      })

    const promiseE = ctx.$axios
      .$get('https://apis.kamontat.net/version/')
      .then(app => {
        commit('setBackendInformation', app)
      })

    await Promise.all([promiseA, promiseB, promiseC, promiseD, promiseE])
  }
}
