import axios from 'axios'

const pkg = require('./package')

const env = process.env.NODE_ENV
const url =
  env === 'production' ? 'https://beta.kamontat.net' : 'http://localhost:3000'

const title = 'Kamontat Chantrachirathumrong'

module.exports = {
  mode: 'universal',
  modern: 'server',
  version: pkg.version,

  /*
   ** Headers of the page
   */
  head: {
    title: title,
    meta: [
      {
        charset: 'utf-8'
      },
      {
        hid: 'content-type',
        'http-equiv': 'content-type',
        content: 'text/html; charset=UTF-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      },
      {
        hid: 'author',
        name: 'author',
        content: pkg.author
      },
      {
        hid: 'version',
        name: 'version',
        content: pkg.version
      },
      {
        hid: 'robots',
        name: 'robots',
        content: 'noarchive'
      },
      {
        hid: 'googlebot',
        name: 'googlebot',
        content: 'noarchive'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ],
    script: [
      {
        src:
          'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
        body: true
      }
    ]
  },

  env: {
    nodeEnv: env,
    baseUrl: url,
    buildDate: new Date().getTime(),
    pkg: pkg
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/focusable.js',
    '~/plugins/axios.js',
    '~/plugins/cloudinary.js',
    '~/plugins/gallery.js',
    '~/plugins/observe.js',
    {
      src: '~/plugins/notification.js',
      mode: 'client'
    }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/moment',
    '@nuxtjs/markdownit',
    '@nuxtjs/google-analytics',
    [
      'cookie-universal-nuxt',
      {
        alias: 'cookie',
        parseJSON: false
      }
    ],
    [
      'vue-scrollto/nuxt',
      {
        duration: 600,
        easing: 'ease-in-out',
        force: true,
        cancelable: false
      }
    ],
    [
      'nuxt-i18n',
      {
        vueI18n: {
          silentTranslationWarn: true
        },
        baseUrl: url,
        seo: false,
        lazy: true,
        langDir: './lang/',
        detectBrowserLanguage: {
          useCookie: true,
          cookieKey: 'kci18n'
        },
        defaultLocale: 'en',
        // vueI18nLoader: true,
        locales: [
          {
            code: 'en',
            name: 'English',
            iso: 'en-US',
            file: 'en.js'
          },
          {
            code: 'th',
            name: 'Thai',
            iso: 'th-TH',
            file: 'th.js'
          }
        ]
      }
    ]
  ],

  /*
   ** Axios module configuration
   */
  axios: {
    baseURL: 'https://apis.kamontat.net/v1/',
    debug: false
    // See https://github.com/nuxt-community/axios-module#options
  },

  googleAnalytics: {
    id: 'UA-124896160-10'
  },

  moment: {
    locales: ['th'],
    defaultLocale: 'en'
  },

  workbox: {
    offlineAnalytics: true,
    offlineAssets: ['_kc'],
    publicPath: '_kc'
  },

  meta: {
    mobileApp: true,
    mobileAppIOS: true,
    name: title,
    ogHost: url,
    twitterCard: 'summary',
    twitterSite: '@kamontatc',
    twitterCreator: '@kamontatc',
    nativeUI: true
  },

  manifest: {
    name: title,
    short_name: 'Frontport',
    lang: 'en',
    orientationSection: 'any'
  },

  // [optional] markdownit options
  // See https://github.com/markdown-it/markdown-it
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true
  },

  generate: {
    subFolders: false,
    fallback: '404.html',
    routes: function() {
      const baseUrl = 'https://apis.kamontat.net/v1'
      const params = {
        who: 'net'
      }
      const paramsTH = params
      paramsTH.lang = 'th' // add lang=th

      const projectUrl = `${baseUrl}/projects/`
      const projects = axios.get(projectUrl, {
        params
      })
      const projectsTH = axios.get(projectUrl, {
        params: paramsTH
      })

      const workUrl = `${baseUrl}/works/`
      const works = axios.get(workUrl, {
        params
      })
      const worksTH = axios.get(workUrl, {
        params: paramsTH
      })

      const volunteerUrl = `${baseUrl}/volunteers/`
      const volunteers = axios.get(volunteerUrl, {
        params
      })
      const volunteersTH = axios.get(volunteerUrl, {
        params: paramsTH
      })

      return Promise.all([
        projects,
        projectsTH,
        works,
        worksTH,
        volunteers,
        volunteersTH
      ]).then(responses => {
        return responses.reduce((p, c, i) => {
          let path = 'projects'
          if (i === 1) path = 'th/projects'
          else if (i === 2) path = 'works'
          else if (i === 3) path = 'th/works'
          else if (i === 4) path = 'volunteers'
          else if (i === 5) path = 'th/volunteers'

          p.push(
            ...c.data.results.map(obj => {
              return {
                route: `${path}/${obj.id}`,
                payload: obj
              }
            })
          )
          return p
        }, [])
      })
    }
  },

  /*
   ** Router configuration
   */
  router: {
    scrollBehavior: (to, from, save) => {
      return require('~/assets/scripts/scroll-utils.js').hashscrolling(
        to,
        from,
        save
      )
    }
  },

  /*
   ** Build configuration
   */
  build: {
    extractCSS: false,
    publicPath: '/_kc/',
    splitChunks: {
      pages: true,
      commons: true,
      layouts: true
    },

    extend(config, { isDev, isClient, loaders }) {
      config.node = {
        fs: 'empty'
      }

      if (isDev) loaders.cssModules.localIdentName = '[name]__[local]'
      else
        loaders.cssModules.localIdentName =
          '[sha512:contenthash:base64:4]-[sha512:contenthash:hex:4]-[sha256:contenthash:base64:4]-[sha256:contenthash:hex:4]'

      const index = config.module.rules.findIndex(v =>
        v.test.toString().includes('sass')
      )
      const scssRule = config.module.rules[index]

      scssRule.oneOf[0].use[1].options.localIdentName =
        loaders.cssModules.localIdentName
      config.module.rules[index] = scssRule

      // Run ESLint on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
