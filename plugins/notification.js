import Vue from 'vue'
import Notifications from 'vue-notification/dist/ssr.js'
import Velocity from 'velocity-animate'

export default () => {
  Velocity.defaults.easing('easeInOut')

  Vue.use(Notifications, {
    velocity: Velocity
  })
}
