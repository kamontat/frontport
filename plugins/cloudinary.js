import { Cloudinary } from 'cloudinary-core'

export default (ctx, inject) => {
  const cl = Cloudinary.new({
    cloud_name: 'kamontat-net',
    secure: true
  })

  inject('cloudinary', cl)
}
