// import Vue from 'vue'
import velocity from 'velocity-animate'

export default (ctx, inject) => {
  // Vue.use(velocity)
  inject('velocity', velocity)
}
