import { cacheAdapterEnhancer, throttleAdapterEnhancer } from 'axios-extensions'
import LRUCache from 'lru-cache'
const ONE_MONTH = 1000 * 60 * 60 * 24 * 30

const defaultCache = new LRUCache({
  maxAge: ONE_MONTH
})

export default function({ $axios, redirect }) {
  $axios.onRequest(config => {
    let message = 'Making request to '
    if (config.url.startsWith('http')) message += config.url
    else message += config.baseURL + config.url

    if (config.params) message += JSON.stringify(config.params)
    console.log(message)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status) || 1
    redirect(`/display/message`, {
      code: code,
      name: error.name,
      message: error.message
    })
  })

  $axios.setHeader('Access-Control-Allow-Origin', '*')

  // https://github.com/kuitos/axios-extensions
  $axios.defaults.adapter = throttleAdapterEnhancer(
    cacheAdapterEnhancer($axios.defaults.adapter, {
      enabledByDefault: false,
      cacheFlag: 'useCache',
      defaultCache
    })
  )
}
