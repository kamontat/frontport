import Vue from 'vue'
import VueObserveVisibility from 'vue-observe-visibility'

export default () => {
  Vue.use(VueObserveVisibility)
}
