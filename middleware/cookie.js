export default function({ app, store }) {
  const theme = app.$cookie.get('kct')
  if (theme) {
    console.log(`Preload with theme=${theme}`)

    switch (theme) {
      case 'light':
        store.commit('lightTheme')
        break
      case 'dark':
        store.commit('darkTheme')
        break
      default:
        store.commit('lightTheme')
        break
    }
  } else {
    app.$cookie.set('kct', store.state.theme, {
      maxAge: 60 * 60 * 24 * 7
    }) // update theme in cookie
  }
}
